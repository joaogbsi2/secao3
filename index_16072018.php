<?php

$barra = DIRECTORY_SEPARATOR;

require dirname(__FILE__).$barra.'inc'.$barra.'Slim-2.x'.$barra.'Slim'.$barra.'Slim.php';
require dirname(__FILE__).$barra.'inc'.$barra.'config.php';

\Slim\Slim::registerAutoloader();


$app = new \Slim\Slim();



// GET route
$app->get(
    '/',
    function () {
       require_once("view/index.php");
    }
);

$app->get(
    '/videos.php',
    function () {
       require_once("view/videos.php");
    }
);


$app->get(
    '/produtos',
    function(){
       
        $sql = new Sql();
        $data = $sql->SELECT("SELECT * FROM hcode_shop.tb_produtos as p where p.preco_promorcional >0 order by p.preco_promorcional desc;");

        foreach ($data as &$produto) {
            $preco = $produto['preco'];
            $centavos = explode(".", $preco);
            $produto['preco'] = number_format($preco, 0, ",", ".");
            $produto['centavos'] = end($centavos);
            $produto['parcelas'] = 10;
            $produto['valor_parcela'] = number_format($preco /$produto['parcelas'], 2, ",", ".");
            $produto['total']= number_format($preco, 2, ",", ".");
        }

        //var_dump($data);
        

        echo json_encode($data);
    }    
);


$app->get(
    '/produtosMaisBuscados',
    function(){
       
        $sql = new Sql();
        $data = $sql->SELECT("
            SELECT 
            hcode_shop.tb_produtos.id_prod,
            hcode_shop.tb_produtos.nome_prod_curto,
            hcode_shop.tb_produtos.nome_prod_longo,
            hcode_shop.tb_produtos.codigo_interno,
            hcode_shop.tb_produtos.id_cat,
            hcode_shop.tb_produtos.preco,
            hcode_shop.tb_produtos.peso,
            hcode_shop.tb_produtos.largura_centimetro,
            hcode_shop.tb_produtos.altura_centimetro,
            hcode_shop.tb_produtos.quantidade_estoque,
            hcode_shop.tb_produtos.preco_promorcional,
            hcode_shop.tb_produtos.foto_principal,
            hcode_shop.tb_produtos.visivel,
            cast(avg(review) as dec(10,2)) as media, 
            count(id_prod) as totalReviews
            FROM hcode_shop.tb_produtos 
            INNER JOIN hcode_shop.tb_reviews USING(id_prod) 
            GROUP BY 
            hcode_shop.tb_produtos.id_prod,
            hcode_shop.tb_produtos.nome_prod_curto,
            hcode_shop.tb_produtos.nome_prod_longo,
            hcode_shop.tb_produtos.codigo_interno,
            hcode_shop.tb_produtos.id_cat,
            hcode_shop.tb_produtos.preco,
            hcode_shop.tb_produtos.peso,
            hcode_shop.tb_produtos.largura_centimetro,
            hcode_shop.tb_produtos.altura_centimetro,
            hcode_shop.tb_produtos.quantidade_estoque,
            hcode_shop.tb_produtos.preco_promorcional,
            hcode_shop.tb_produtos.foto_principal,
            hcode_shop.tb_produtos.visivel
            LIMIT 4;
            ");

        foreach ($data as &$produto) {
            $preco = $produto['preco'];
            $centavos = explode(".", $preco);
            $produto['preco'] = number_format($preco, 0, ",", ".");
            $produto['centavos'] = end($centavos);
            $produto['parcelas'] = 10;
            $produto['valor_parcela'] = number_format($preco /$produto['parcelas'], 2, ",", ".");
            $produto['total']= number_format($preco, 2, ",", ".");             
        }

        //var_dump($data);
        

        echo json_encode($data);
    }    
);



$app->get(
    '/shop.php',
    function () {
       require_once("view/shop.php");
    }
);

$app->get(
    '/produto-:id_prod',
    function ($id_prod) {
        $sql = new Sql();
        $produtos = $sql->select("SELECT * from hcode_shop.tb_produtos WHERE id_prod = $id_prod");
        //var_dump($produto);
        $produto = $produtos[0];

        $preco = $produto['preco'];
        $centavos = explode(".", $preco);
        $produto['preco'] = number_format($preco, 0, ",", ".");
        $produto['centavos'] = end($centavos);
        $produto['parcelas'] = 10;
        $produto['valor_parcela'] = number_format($preco /$produto['parcelas'], 2, ",", ".");
        $produto['total']= number_format($preco, 2, ",", ".");
            
        require_once("view/shop_produto.php");

    }
);

// POST route
$app->post(
    '/post',
    function () {
        echo 'This is a POST route';
    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);


$app->run();
